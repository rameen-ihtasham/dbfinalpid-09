﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBFinalProject.BL
{
    public class Class
    {
        public int ClassID { get; set; }
        public string ClassGrade { get; set; }

        public Class(int classID, string classGrade)
        {
            ClassID = classID;
            ClassGrade = classGrade;
        }
    }
}
