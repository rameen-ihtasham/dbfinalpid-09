﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBFinalProject.BL
{
    public class ClassSubject
    {
        public int ClassID { get; set; }
        public int SubjectID { get; set; }
        public int TeacherID { get; set; }
        public int Position { get; set; }

        public int IsActive { get; set; }

        public ClassSubject(int cId , int sId, int tId , int position , int status)
        {
            this.Position = position;
            this.ClassID = cId;
            this.SubjectID = sId;
            this.TeacherID = tId;
            this.IsActive = status;
        }


    }
}
