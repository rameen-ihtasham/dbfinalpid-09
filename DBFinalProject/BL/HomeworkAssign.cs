﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using DBFinalProject.Forms;
namespace DBFinalProject.BL
{
    public class HomeworkAssign
    {
        public int homeworkID { get; set; }

        public int TeacherID { get; set; }
        public int subjectId { get; set; }

        public DateTime date { get; set; }
        public string HomeWork { get; set; }

        public DateTime UpdatedOn { get; set; }

        public int UpdatedBy { get; set; }
        public HomeworkAssign()
        {
        }
        public HomeworkAssign(int TeacherID, int subjectId, DateTime date, string Homework, DateTime UpdatedOn, int UpdatedBy)
        {
           
            this.TeacherID = TeacherID;
            this.subjectId = subjectId;
            this.date = date;
            this.HomeWork = HomeWork;
            this.UpdatedOn = UpdatedOn;
            this.UpdatedBy = UpdatedBy;




        }

    }
}
