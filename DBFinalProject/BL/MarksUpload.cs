﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace DBFinalProject.BL
{
    public class MarksUpload
    {

        public int ID { get; set; }
        public int StID { get; set; }

        public int SubID { get; set; }

        public int TeacheID { get; set; }
        public string Session { get; set; }

        public string Term { get; set; }

        public float ObtainedMarks{ get; set; }
        public MarksUpload()
        {
            
        }
        public MarksUpload(int StID, int SubID, int TeacheID, string Session, string Term , float ObtainedMarks )

        {
            this.StID = StID;
            this.SubID = SubID;
            this.TeacheID = TeacheID;
            this.Session = Session;
            this.Term = Term;
            this.ObtainedMarks = ObtainedMarks;
        }


    }

}
