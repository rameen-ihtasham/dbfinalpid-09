﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBFinalProject.BL
{
    public class Person
    {
        public int PersonID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Contact { get; set; }
        public string Email { get; set; }
        public DateTime DOB { get; set; }
        public string Address { get; set; }
        public string CNIC { get; set; }
        public int Gender { get; set; }
        public int Status { get; set; }
        public int Age { get; set; }
        public int UserId { get; set; }

        public Person(int personID, string firstName, string lastName, string contact, string email, DateTime dob, string address, string cnic, int gender, int status, int age, int userId)
        {
            PersonID = personID;
            FirstName = firstName;
            LastName = lastName;
            Contact = contact;
            Email = email;
            DOB = dob;
            Address = address;
            CNIC = cnic;
            Gender = gender;
            Status = status;
            Age = DateTime.Today.Year - dob.Year;
            UserId = userId;
        }

        public Person(string firstName, string lastName, string contact, string email, DateTime dob, string address, string cnic, int gender, int status, int age)
        {

            FirstName = firstName;
            LastName = lastName;
            Contact = contact;
            Email = email;
            DOB = dob;
            Address = address;
            CNIC = cnic;
            Gender = gender;
            Status = status;
            Age = age;

        }




    }
}
