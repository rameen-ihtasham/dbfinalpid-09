﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBFinalProject.BL
{
    public class Student
    {
        public int StdID { get; set; }
        public string RegistrationNumber { get; set; }

        public Student(int stdID, string registrationNumber)
        {
            StdID = stdID;
            RegistrationNumber = registrationNumber;
        }
        public Student(string registrationNumber)
        {

            RegistrationNumber = registrationNumber;
        }
    }
}
