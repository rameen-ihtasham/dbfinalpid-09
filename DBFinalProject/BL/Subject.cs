﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBFinalProject.BL
{
    public class Subject
    {
        public int subID { get; set; }
        public string subName { get; set; }
        public int subYear { get; set; }
        public int isActive { get; set; }



        public Subject(int id , string name, int year , int  active)
        {
            this.subID = id;
            this.subName = name;
            this.subYear = year;
            this.isActive = active;
        }
    }
}
