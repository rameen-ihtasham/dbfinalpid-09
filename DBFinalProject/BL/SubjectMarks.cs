﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBFinalProject.BL
{
    public class SubjectMarks
    {
        public int subid;
        public int term;
        public int marks;

        public SubjectMarks(int id , int term , int marks) { 
            this.subid = id;    
            this.term = term;
            this.marks = marks;
        }

    }
}
