﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBFinalProject.BL
{
    public class Teacher
    {
        public int TeachID { get; set; }
        public int Designation { get; set; }

        public Teacher(int teachID, int designation)
        {
            TeachID = teachID;
            Designation = designation;
        }
    }
}
