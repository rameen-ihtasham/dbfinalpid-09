﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBFinalProject.BL
{
    public class User
    {
        public int ID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int Role { get; set; }

        public User(int id, string userName, string password, int role)
        {
            ID = id;
            UserName = userName;
            Password = password;
            Role = role;
        }
        public User() { }
        public User(string userName, string password, int role)
        {

            UserName = userName;
            Password = password;
            Role = role;
        }

        public User(string userName, string password)
        {

            UserName = userName;
            Password = password;

        }
    }
}
