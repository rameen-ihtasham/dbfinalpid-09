﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBFinalProject.BL
{
    public class attendanceMark
    {
        public int StudentID { get; set; }

        public int ClassID { get; set; }
        public int TeacherID { get; set; }

        public DateTime date { get; set; }
        public int IsPresent { get; set; }
        public attendanceMark()
        {
        }
        public attendanceMark(int stdid, DateTime date , int classid, int teacherid, int ispresent)
        {
            this.StudentID = stdid;
            this.date = date;
            this.ClassID = classid;
            this.TeacherID = teacherid;
            this.IsPresent = ispresent;
        }



    }
}
