﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using DBFinalProject.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBFinalProject.CrystalReports
{
    public partial class GenerateResultReport : Form
    {
        public GenerateResultReport()
        {
            InitializeComponent();
        }

        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {
            ReportDocument reportDocument = new ReportDocument();

            // Load the Crystal Report file (.rpt) into the reportDocument object
            reportDocument.Load("C:\\Users\\Alishba Mazhar\\dbfinalpid-09\\DBFinalProject\\CrystalReports\\Result.rpt"); // Replace with your report file path

            ParameterValues parameterValues = new ParameterValues();
            ParameterDiscreteValue parameterDiscreteValue = new ParameterDiscreteValue();

            // Assuming 'paramName' is the parameter name defined in your Crystal Report
            parameterDiscreteValue.Value = ResultReport.mlId;

            // Add the parameter value to the parameter values collection
            parameterValues.Add(parameterDiscreteValue);

            // Apply the parameter values to the report document
            reportDocument.SetParameterValue("ID", parameterValues);

            // Set the report source for the Crystal Report Viewer
            crystalReportViewer1.ReportSource = reportDocument;
        }
    }
}
