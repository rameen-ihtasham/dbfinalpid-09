﻿using System;
using System.Data.SqlClient;

namespace DBFinalProject
{
    class Configuration : IDisposable
    {
        SqlConnection con;
        string ConnectionStr = @"Server=.; Database=DBFinal; Trusted_Connection = True";
        private static Configuration _instance;

        public static Configuration getInstance()
        {
            if (_instance == null)
                _instance = new Configuration();
            return _instance;
        }

        private Configuration()
        {
            try
            {
                con = new SqlConnection(ConnectionStr);
                con.Open();
            }
            catch (Exception ex)
            {
                // Handle connection error
                Console.WriteLine("Error opening connection: " + ex.Message);
                // Optionally: throw an exception or take appropriate action
            }
        }

        public SqlConnection getConnection()
        {
            // Ensure the connection is open before returning
            if (con.State != System.Data.ConnectionState.Open)
            {
                try
                {
                    con.Open();
                }
                catch (Exception ex)
                {
                    // Handle connection error
                    Console.WriteLine("Error opening connection: " + ex.Message);
                    // Optionally: throw an exception or take appropriate action
                }
            }
            return con;
        }

        // Dispose method to properly release resources
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (con != null)
                {
                    con.Dispose();
                }
            }
        }
    }
}
