﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBFinalProject.DataAccess
{
    public static class Dictionaries
    {
        public static Dictionary<int, string> genders = new Dictionary<int, string>() {
            {3, "Male" },
            {4, "Female" }
        };

        public static Dictionary<int, string> desginations = new Dictionary<int, string>()
        {

            {13,"PST" },
            {14,"EST" },
            {15,"SST" }
        };

        public static Dictionary<int, string> subjectTerms = new Dictionary<int, string>()
        {
            {16,"1st" },
            {17,"2nd" },
            {18,"3rd" }
        };

        public static Dictionary<int, string> positions = new Dictionary<int, string>()
        {
            {11,"Incharge" },
            {10,"Lecturer" }
        };

    }
}
