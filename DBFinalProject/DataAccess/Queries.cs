﻿using DBFinalProject.DataAccess;
using DBFinalProject.BL;
using DBFinalProject.Forms;
using Guna.UI2.WinForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Management;
using System.Windows.Forms;
using System.Data.SqlTypes;
using System.Security.Cryptography.X509Certificates;

namespace DBFinalProject.DataAccess
{
    class Queries
    {

        public static SqlConnection con = Configuration.getInstance().getConnection();

        public static int LastInsertedUserId { get; private set; }

        static public void InsertUser(User user)
        {
            SqlCommand cmd = new SqlCommand("INSERT INTO [User] ( UserName, Password, Role) OUTPUT Inserted.ID VALUES ( @Username, @Password, @role);", con);

            cmd.Parameters.AddWithValue("@Username", user.UserName);
            cmd.Parameters.AddWithValue("@Password", user.Password);
            cmd.Parameters.AddWithValue("@role", user.Role);
            LastInsertedUserId = Convert.ToInt32(cmd.ExecuteScalar());
            cmd.ExecuteNonQuery();

        }

        static public DataTable GetStudentsData()
        {
            SqlCommand cmd = new SqlCommand("select * from activeStudentsView;", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        static public Person GetStudentAttributes(int id)
        {
            SqlCommand cmd = new SqlCommand("SELECT PersonID,FirstName, LastName, Contact, Email, DOB, Address, CNIC, Gender, Status, Age, UserId FROM Person WHERE PersonID = @PersonID", con);
            cmd.Parameters.AddWithValue("@PersonID", id);
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                Person p = new Person
                (
                    Convert.ToInt32(reader["PersonID"]),
                    reader["FirstName"].ToString(),
                    reader["LastName"].ToString(),
                    reader["Contact"].ToString(),
                    reader["Email"].ToString(),
                    Convert.ToDateTime(reader["DOB"]),
                    reader["Address"].ToString(),
                    reader["CNIC"].ToString(),
                    Convert.ToInt32(reader["Gender"]),
                    Convert.ToInt32(reader["Status"]),
                    Convert.ToInt32(reader["Age"]),
                    Convert.ToInt32(reader["UserId"])
                );
                reader.Close();
                return p;
            }
            return null;
        }

        static public DataTable GetTeachersData()
        {
            SqlCommand cmd = new SqlCommand("select * from activeTeachersView;", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        static public DataTable GetClassesData()
        {
            SqlCommand cmd = new SqlCommand("select * from classView;", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        static public DataTable GetSubjectsData()
        {
            SqlCommand cmd = new SqlCommand("select * from subjectsView;", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        public static void DeleteStudent(int Id)
        {
            SqlCommand cmd = new SqlCommand("EXEC spDeleteStudent @id =@ID ;", con);
            cmd.Parameters.AddWithValue("@ID", Id);
            cmd.ExecuteNonQuery();

        }

        public static void updateStudent(Person p)
        {
            SqlCommand cmd = new SqlCommand("EXEC spUpdateStudent @fName =@FN , @lName = @LN , @CNIC = @CNICC , @Contact = @contact , @Gender = @gender , @dateOfBirth = @dob , @address = @add , @email = @EMAIL,@age = @AGE, @id = @ID", con);
            cmd.Parameters.AddWithValue("@ID", p.PersonID);
            cmd.Parameters.AddWithValue("@FN", p.FirstName);
            cmd.Parameters.AddWithValue("@LN", p.LastName);
            cmd.Parameters.AddWithValue("@CNICC", p.CNIC);
            cmd.Parameters.AddWithValue("@contact", p.Contact);
            cmd.Parameters.AddWithValue("@gender ", p.Gender);
            cmd.Parameters.AddWithValue("@dob", p.DOB);
            cmd.Parameters.AddWithValue("@add", p.Address);
            cmd.Parameters.AddWithValue("@EMAIL", p.Email);
            cmd.Parameters.AddWithValue("@AGE", System.DateTime.Today.Year - p.DOB.Year);
            cmd.ExecuteNonQuery();

        }

        public static void AddTeacher(User u, Person p, Teacher t)
        {
            using (SqlTransaction transaction = con.BeginTransaction())
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("insert into [User] output inserted.ID values(@userName , @password ,@role );", con, transaction);
                    cmd.Parameters.AddWithValue("@userName", u.UserName);
                    cmd.Parameters.AddWithValue("@password", u.Password);
                    cmd.Parameters.AddWithValue("@role", u.Role);
                    int userID = (int)cmd.ExecuteScalar();


                    SqlCommand cmd1 = new SqlCommand("insert into Person output inserted.PersonID values(@fName , @lName, @phone , @email ,@dob ,@address ,@cnic, @gender , @status , @age ,@userId);", con, transaction);
                    cmd1.Parameters.AddWithValue("@fName", p.FirstName);
                    cmd1.Parameters.AddWithValue("@lName", p.LastName);
                    cmd1.Parameters.AddWithValue("@phone", p.Contact);
                    cmd1.Parameters.AddWithValue("@email", p.Email);
                    cmd1.Parameters.AddWithValue("@dob", p.DOB);
                    cmd1.Parameters.AddWithValue("@address", p.Address);
                    cmd1.Parameters.AddWithValue("@cnic", p.CNIC);
                    cmd1.Parameters.AddWithValue("@gender", p.Gender);
                    cmd1.Parameters.AddWithValue("@status", p.Status);
                    cmd1.Parameters.AddWithValue("@age", p.Age);
                    cmd1.Parameters.AddWithValue("@userId", userID);
                    int personID = (int)cmd1.ExecuteScalar();

                    SqlCommand cmd2 = new SqlCommand("insert into [Teacher] values(@ID,@Designation); ", con, transaction);
                    cmd2.Parameters.AddWithValue("@ID", personID);
                    cmd2.Parameters.AddWithValue("@Designation", t.Designation);
                    cmd2.ExecuteNonQuery();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    transaction.Rollback();
                }
            }


        }

        static public Person GetPersonAttributes(int id)
        {
            SqlCommand cmd = new SqlCommand("SELECT PersonID,FirstName, LastName, Contact, Email, DOB, Address, CNIC, Gender, Status, Age, UserId FROM Person WHERE PersonID = @PersonID", con);
            cmd.Parameters.AddWithValue("@PersonID", id);
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                Person p = new Person
                (
                    Convert.ToInt32(reader["PersonID"]),
                    reader["FirstName"].ToString(),
                    reader["LastName"].ToString(),
                    reader["Contact"].ToString(),
                    reader["Email"].ToString(),
                    Convert.ToDateTime(reader["DOB"]),
                    reader["Address"].ToString(),
                    reader["CNIC"].ToString(),
                    Convert.ToInt32(reader["Gender"]),
                    Convert.ToInt32(reader["Status"]),
                    Convert.ToInt32(reader["Age"]),
                    Convert.ToInt32(reader["UserId"])
                );
                reader.Close();
                return p;
            }
            return null;
        }

        static public Teacher GetTeacherAttributes(int id)
        {
            SqlCommand cmd = new SqlCommand("select Designation from Person join teacher on Person.PersonID = Teacher.teachID where personID = @ID;", con);
            cmd.Parameters.AddWithValue("@ID", id);
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                Teacher t = new Teacher
                (
                    id,
                    Convert.ToInt32(reader["Designation"])

                );
                reader.Close();
                return t;
            }
            return null;
        }

        public static void UpdateTeacher(Teacher teacher, Person p)
        {

            using (SqlTransaction transaction = con.BeginTransaction())
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("EXEC spUpdateStudent @fName =@FN , @lName = @LN , @CNIC = @CNICC , @Contact = @contact , @Gender = @gender , @dateOfBirth = @dob , @address = @add , @email = @EMAIL,@age = @AGE, @id = @ID", con, transaction);
                    cmd.Parameters.AddWithValue("@ID", p.PersonID);
                    cmd.Parameters.AddWithValue("@FN", p.FirstName);
                    cmd.Parameters.AddWithValue("@LN", p.LastName);
                    cmd.Parameters.AddWithValue("@CNICC", p.CNIC);
                    cmd.Parameters.AddWithValue("@contact", p.Contact);
                    cmd.Parameters.AddWithValue("@gender ", p.Gender);
                    cmd.Parameters.AddWithValue("@dob", p.DOB);
                    cmd.Parameters.AddWithValue("@add", p.Address);
                    cmd.Parameters.AddWithValue("@EMAIL", p.Email);
                    cmd.Parameters.AddWithValue("@AGE", System.DateTime.Today.Year - p.DOB.Year);
                    cmd.ExecuteNonQuery();

                    SqlCommand cmd1 = new SqlCommand("update teacher set Designation = @Desig where teachID = @ID ", con, transaction);
                    cmd1.Parameters.AddWithValue("@Desig", teacher.Designation);
                    cmd1.Parameters.AddWithValue("ID", teacher.TeachID);
                    cmd1.ExecuteNonQuery();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    transaction.Rollback();
                }
            }
        }

        public static List<int> GetAvailableClasses()
        {
            List<int> list = new List<int>();
            SqlCommand cmd = new SqlCommand("select * from availableClassesView;", con);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                int classNumber = reader.GetInt32(0);
                list.Add(classNumber);
            }
            reader.Close();
            return list;

        }
        public static void AddClass(int classNumber)
        {
            SqlCommand cmd = new SqlCommand("insert into Class values(@number);", con);
            cmd.Parameters.AddWithValue("@number", classNumber);
            cmd.ExecuteNonQuery();
        }

        public static void AddSubject(Subject subject, SubjectMarks smOne, SubjectMarks smTwo, SubjectMarks smThree)
        {
            using (SqlTransaction transaction = con.BeginTransaction())
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("insert into Subject output inserted.subjectID values(@name,@year,@active)", con, transaction);
                    cmd.Parameters.AddWithValue("@name", subject.subName);
                    cmd.Parameters.AddWithValue("@year", subject.subYear);
                    cmd.Parameters.AddWithValue("@active", subject.isActive);
                    int id = (int)cmd.ExecuteScalar();


                    SqlCommand cmd1 = new SqlCommand("insert into SubjectMarks values(@id , @term , @marks)", con, transaction);
                    cmd1.Parameters.AddWithValue("@id", id);
                    cmd1.Parameters.AddWithValue("@term", smOne.term);
                    cmd1.Parameters.AddWithValue("@marks", smOne.marks);
                    cmd1.ExecuteNonQuery();


                    SqlCommand cmd2 = new SqlCommand("insert into SubjectMarks values(@id , @term , @marks)", con, transaction);
                    cmd2.Parameters.AddWithValue("@id", id);
                    cmd2.Parameters.AddWithValue("@term", smTwo.term);
                    cmd2.Parameters.AddWithValue("@marks", smTwo.marks);
                    cmd2.ExecuteNonQuery();

                    SqlCommand cmd3 = new SqlCommand("insert into SubjectMarks values(@id , @term , @marks)", con, transaction);
                    cmd3.Parameters.AddWithValue("@id", id);
                    cmd3.Parameters.AddWithValue("@term", smThree.term);
                    cmd3.Parameters.AddWithValue("@marks", smThree.marks);
                    cmd3.ExecuteNonQuery();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    transaction.Rollback();
                }
            }

        }

        public static void DeleteSubject(int id)
        {
            using (SqlTransaction transaction = con.BeginTransaction())
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("update subject set IsActive = 6 where subjectID  =  @ID;", con, transaction);
                    cmd.Parameters.AddWithValue("@ID", id);
                    cmd.ExecuteNonQuery();

                    SqlCommand cmd1 = new SqlCommand("update ClassSubject set IsActive = 6  where SubjectID = @ID;", con, transaction);
                    cmd1.Parameters.AddWithValue("@ID", id);
                    cmd1.ExecuteNonQuery();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    transaction.Rollback();
                }
            }
        }

        public static int getSubjectMarks(int term, int subjectID)
        {
            int marks = -1;
            SqlCommand cmd = new SqlCommand("select TotalMarks from SubjectMarks where SubjectMarks.subID = @id and SubjectMarks.Term = @term", con);
            cmd.Parameters.AddWithValue("@id", subjectID);
            cmd.Parameters.AddWithValue("@term", term);
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                marks = Convert.ToInt32(reader["TotalMarks"]);
            }
            reader.Close();
            return marks;


        }

        public static void UpdateSubject(int id, string newName, SubjectMarks subjectMarksOne, SubjectMarks subjectMarksTwo, SubjectMarks subjectMarksThree)
        {
            using (SqlTransaction transaction = con.BeginTransaction())
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("update subject set subjectName = @name where subjectID  =  @ID;", con, transaction);
                    cmd.Parameters.AddWithValue("@ID", id);
                    cmd.Parameters.AddWithValue("@name", newName);
                    cmd.ExecuteNonQuery();

                    SqlCommand cmd1 = new SqlCommand("update SubjectMarks set TotalMarks = @marks where subID = @id AND Term = @term", con, transaction);
                    cmd1.Parameters.AddWithValue("@marks", subjectMarksOne.marks);
                    cmd1.Parameters.AddWithValue("@id", subjectMarksOne.subid);
                    cmd1.Parameters.AddWithValue("@term", subjectMarksOne.term);
                    cmd1.ExecuteNonQuery();

                    SqlCommand cmd2 = new SqlCommand("update SubjectMarks set TotalMarks = @marks where subID = @id AND Term = @term", con, transaction);
                    cmd2.Parameters.AddWithValue("@marks", subjectMarksTwo.marks);
                    cmd2.Parameters.AddWithValue("@id", subjectMarksTwo.subid);
                    cmd2.Parameters.AddWithValue("@term", subjectMarksTwo.term);
                    cmd2.ExecuteNonQuery();

                    SqlCommand cmd3 = new SqlCommand("update SubjectMarks set TotalMarks = @marks where subID = @id AND Term = @term", con, transaction);
                    cmd3.Parameters.AddWithValue("@marks", subjectMarksThree.marks);
                    cmd3.Parameters.AddWithValue("@id", subjectMarksThree.subid);
                    cmd3.Parameters.AddWithValue("@term", subjectMarksThree.term);
                    cmd3.ExecuteNonQuery();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    transaction.Rollback();
                }
            }
        }

        public static void UpdateSubjectMarks(int id, int term, int marks)
        {
            SqlCommand cmd = new SqlCommand("update SubjectMarks set TotalMarks = @marks where subID = @id AND Term = @term", con);
            cmd.Parameters.AddWithValue("@marks", marks);
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Parameters.AddWithValue("@term", term);
            cmd.ExecuteNonQuery();
        }

        public static Dictionary<int, string> GetClassesDictionary()
        {
            Dictionary<int, string> classes = new Dictionary<int, string>();
            SqlCommand sqlCommand = new SqlCommand("select * from Class order by ClassGrade", con);
            SqlDataReader dataReader = sqlCommand.ExecuteReader();
            while (dataReader.Read())
            {
                int classId = dataReader.GetInt32(0);
                string classGrade = dataReader.GetString(1);
                classes.Add(classId, classGrade);

            }
            dataReader.Close();
            return classes;
        }

        public static Dictionary<int, string> GetSubjectDictionary()
        {
            Dictionary<int, string> subjects = new Dictionary<int, string>();
            SqlCommand sqlCommand = new SqlCommand("select subjectID , subjectName from Subject where IsActive = 5;", con);
            SqlDataReader dataReader = sqlCommand.ExecuteReader();
            while (dataReader.Read())
            {
                int classId = dataReader.GetInt32(0);
                string classGrade = dataReader.GetString(1);
                subjects.Add(classId, classGrade);

            }
            dataReader.Close();
            return subjects;
        }

        public static Dictionary<int, string> GetTeacherDictionary()
        {
            Dictionary<int, string> teacher = new Dictionary<int, string>();
            SqlCommand sqlCommand = new SqlCommand("select teachID , FirstName+ ' ' +LastName as Name from Teacher join Person on teachID = PersonID Where status = 5;", con);
            SqlDataReader dataReader = sqlCommand.ExecuteReader();
            while (dataReader.Read())
            {
                int classId = dataReader.GetInt32(0);
                string classGrade = dataReader.GetString(1);
                teacher.Add(classId, classGrade);

            }
            dataReader.Close();
            return teacher;
        }

        public static void AddClassTeacher(int classId, int teacherId, int position)
        {
            SqlCommand sqlCommand = new SqlCommand("insert into ClassTeacher values (@cId , @tId, @position);", con);
            sqlCommand.Parameters.AddWithValue("@cId", classId);
            sqlCommand.Parameters.AddWithValue("@tId", teacherId);
            sqlCommand.Parameters.AddWithValue("@position", position);
            sqlCommand.ExecuteNonQuery();
        }
        //public static void AddClassSubject(ClassSubject cs)
        //{
        //    SqlCommand sqlCommand = new SqlCommand("insert into ClassSubject values (@cId , @sId , @tId , @position , @status);", con);
        //    sqlCommand.Parameters.AddWithValue("@cId", cs.ClassID);
        //    sqlCommand.Parameters.AddWithValue("@sId", cs.SubjectID);
        //    sqlCommand.Parameters.AddWithValue("@tId", cs.TeacherID);
        //    sqlCommand.Parameters.AddWithValue("@position", cs.Position);
        //    sqlCommand.Parameters.AddWithValue("@status", cs.IsActive);
        //    sqlCommand.ExecuteNonQuery();
        //}

        static public DataTable GetClassSubjectsData()
        {
            SqlCommand cmd = new SqlCommand("select ClassGrade , subjectName , FirstName+ ' ' + LastName as TeacherName , Lookup.Value as Position from ClassSubject join Class on Class.classID = ClassSubject.ClassID  join [Subject] on ClassSubject.SubjectID = Subject.subjectID join person on person.PersonID = ClassSubject.TeacherID join Lookup on ClassSubject.Position = Lookup.lookupID where ClassSubject.IsActive = 5;", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        static public Dictionary<int, string> GetSubjectsForClass(int classID)
        {
            Dictionary<int, string> subjects = new Dictionary<int, string>();
            using (SqlCommand cmd = new SqlCommand("SELECT subjectID, subjectName FROM Subject WHERE subjectID NOT IN (SELECT subjectID FROM ClassSubject WHERE ClassID = @id) AND IsActive = 5;", con))
            {
                cmd.Parameters.AddWithValue("@id", classID);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        subjects.Add(reader.GetInt32(0), reader.GetString(1));
                    }
                }
            }
            return subjects;
        }

        public static bool ClassHasIncharge(int classID)
        {
            SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM ClassSubject where ClassID = @id AND Position = (SELECT lookupID FROM Lookup WHERE Value = 'Incharge')", con);
            cmd.Parameters.AddWithValue("@id", classID);
            return ((int)cmd.ExecuteScalar()) > 0;
        }

        public static void AddClassSubject(ClassSubject cs)
        {
            string query = @"
                    DECLARE @RowCount INT;
                    SELECT @RowCount = COUNT(*) FROM ClassSubject WHERE ClassID = @classID AND SubjectID = @subjectID;
                    IF (@RowCount = 0)
                    BEGIN 
                        INSERT INTO ClassSubject VALUES (@classId , @subjectId , @teacherId , @position, 5);
                    END
                    ELSE
                    BEGIN
                        UPDATE ClassSubject SET TeacherID = @teacherId, Position = @position WHERE ClassID = @classId AND SubjectID = @subjectId;
                    END
                    ";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@classId", cs.ClassID);
            cmd.Parameters.AddWithValue("@subjectId", cs.SubjectID);
            cmd.Parameters.AddWithValue("@teacherId", cs.TeacherID);
            cmd.Parameters.AddWithValue("@position", cs.Position);
            cmd.ExecuteNonQuery();
        }

        public static bool IsClassTeacher(int id)
        {
            bool isPresent = false;
            SqlCommand sqlCommand = new SqlCommand("select count(*) from ClassSubject where TeacherID = @ID group by TeacherID ;", con);
            sqlCommand.Parameters.AddWithValue("@ID", id);
            SqlDataReader reader = sqlCommand.ExecuteReader();
            if (reader.Read())
            {
                int count = reader.GetInt32(0);
                if (count > 0)
                {
                    isPresent = true;
                }
            }
            reader.Close();
            return isPresent;
        }

        public static void DeleteTeacher(int id)
        {
            using (SqlTransaction transaction = con.BeginTransaction())
            {
                try
                {
                    SqlCommand sqlCommand = new SqlCommand("update Person set Status  = 6 where PersonID = @ID;", con, transaction);
                    sqlCommand.Parameters.AddWithValue("@ID", id);
                    sqlCommand.ExecuteNonQuery();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    transaction.Rollback();

                }
            }
        }

        public static Dictionary<int, string> GetAlternateTeachersDictionary(int id)
        {
            Dictionary<int, string> result = new Dictionary<int, string>();
            SqlCommand sqlCommand = new SqlCommand("select PersonID , FirstName + ' ' + LastName as name from Teacher join Person on teachID = PersonID where PersonID <> @ID AND Status = 5;", con);
            sqlCommand.Parameters.AddWithValue("@ID", id);
            SqlDataReader reader = sqlCommand.ExecuteReader();
            while (reader.Read())
            {
                int Id = reader.GetInt32(0);
                string name = reader.GetString(1);
                result.Add(Id, name);

            }
            reader.Close();
            return result;
        }

        public static void AssignAlternateTeacher(int oldTeacherId, int newTeacherId)
        {
            SqlCommand sqlCommand = new SqlCommand("update ClassSubject set TeacherID = @ID where TeacherID = @oldID;", con);
            sqlCommand.Parameters.AddWithValue("@ID", newTeacherId);
            sqlCommand.Parameters.AddWithValue("@oldID", oldTeacherId);
            sqlCommand.ExecuteNonQuery();
        }

        public static bool IsSubjectPresent(string name)
        {
            bool result = false;
            SqlCommand cmd = new SqlCommand("select Count(*) from Subject where subjectName = @Name and IsActive = 5;", con);
            cmd.Parameters.AddWithValue("@Name", name);
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                int count = reader.GetInt32(0);
                if (count > 0)
                {
                    result = true;
                }
            }
            reader.Close();
            return result;
        }

        public static bool IsSelfSubjectPresent(string name, int id)
        {
            bool result = false;
            SqlCommand cmd = new SqlCommand("select Count(*) from Subject where subjectName = @Name and subjectID <> @id and IsActive = 5;", con);
            cmd.Parameters.AddWithValue("@Name", name);
            cmd.Parameters.AddWithValue("@id", id);
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                int count = reader.GetInt32(0);
                if (count > 0)
                {
                    result = true;
                }
            }
            reader.Close();
            return result;
        }

        static public void InsertStudentUser(User user, Student stu, Person student, int rClassValue)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("stp_InsertStudentUser", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Username", user.UserName);
                cmd.Parameters.AddWithValue("@Password", user.Password);
                cmd.Parameters.AddWithValue("@Role", user.Role);
                cmd.Parameters.AddWithValue("@FirstName", student.FirstName);
                cmd.Parameters.AddWithValue("@LastName", student.LastName);
                cmd.Parameters.AddWithValue("@Contact", student.Contact);
                cmd.Parameters.AddWithValue("@Email", student.Email);
                cmd.Parameters.AddWithValue("@DOB", student.DOB);
                cmd.Parameters.AddWithValue("@Gender", student.Gender);
                cmd.Parameters.AddWithValue("@Address", student.Address);
                cmd.Parameters.AddWithValue("@CNIC", student.CNIC);
                cmd.Parameters.AddWithValue("@Status", student.Status);
                cmd.Parameters.AddWithValue("@Age", student.Age);
                cmd.Parameters.AddWithValue("@ClassGrade", rClassValue);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Student Registered Successfully");
            }
            catch (Exception ex)
            {

                MessageBox.Show($"An error occurred: {ex.Message}");

            }
        }
        public static DataTable ShowhomeWorkData(int id)
        {
            DataTable dt = new DataTable();

            try
            {
                string query = @"
SELECT Subject.subjectName, Person.FirstName + ' ' + Person.LastName AS TeacherName, Homework.HomeWork FROM Homework
JOIN ClassStudent ON ClassStudent.ClasID = Homework.classId
JOIN Subject ON Subject.subjectID = Homework.subjectId
JOIN Class ON Class.classID = HomeWork.classId
JOIN Person ON Person.PersonID = Homework.TeacherID
WHERE ClassStudent.StudentID = @studentId;
";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@studentId", id);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                if (dt.Rows.Count == 0)
                {
                    MessageBox.Show("No Homework is uploaded for this subject.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"An error occurred: {ex.Message}");
            }
            return dt;
        }

        public static DataTable ShowAttendance(int sId)
        {
            DataTable dt = new DataTable();

            try
            {
                SqlCommand cmd = new SqlCommand(@"SELECT a.StudentID,p.FirstName+' '+p.LastName as StudentName,a.Date,a.ClassID,c.ClassGrade,a.TeacherID,l.Value as IsPresent
                                                FROM Attendance a join Lookup l on l.lookupID=a.IsPresent
                                                join Person p on p.PersonID=a.StudentID
                                                join Class c on c.classID=a.ClassID
                                                where a.StudentID=@studentid", con);
                cmd.Parameters.AddWithValue("@studentid", sId);
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);
                if (dt.Rows.Count == 0)
                {
                    MessageBox.Show("No attendance data found for the current user.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"An error occurred: {ex.Message}");
            }
            return dt;


        }
        public static void InsertResult(int teacherId, int subjectId, int studentid, string term, string session, float obtain)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("stp_InsertResult", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@stId", studentid);
                cmd.Parameters.AddWithValue("@subId", subjectId);
                cmd.Parameters.AddWithValue("@TeacheID", teacherId);
                cmd.Parameters.AddWithValue("@Session", session);
                cmd.Parameters.AddWithValue("@Term", term);
                cmd.Parameters.AddWithValue("@ObtainedMarks", obtain);

                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred: " + ex.Message);

            }

        }

        public static float GetTotalMarksForClass()
        {
            float totalMarks = 0.0f;

            try
            {
                SqlCommand cmd = new SqlCommand("SELECT n.TotalMarks FROM ClassSubject c JOIN SubjectMarks n ON c.subID = n.subID WHERE c.ClaassID = @ClassID;", con);
                cmd.Parameters.AddWithValue("@ClassID", 3);


                using (SqlDataReader reader = cmd.ExecuteReader())

                {
                    if (reader.Read())
                    {
                        totalMarks = Convert.ToSingle(reader["TotalMarks"]);

                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);

            }

            return totalMarks;
        }
        public static DataTable ShowAttendancemonth(int sId)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand(@"stp_ShowAttendanceMonthly", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@studentid", sId);
                cmd.Parameters.AddWithValue("@month", VIewAttendance.month);
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);
                if (dt.Rows.Count == 0)
                {
                    MessageBox.Show("No attendance data found for the current month .", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
            return dt;

        }


        public static DataTable ShowAttendanceMonthwise(int sId)
        {
            DataTable dt = new DataTable();
            try
            {

                SqlCommand cmd = new SqlCommand(@"SELECT a.StudentID,p.FirstName+' '+p.LastName as StudentName ,SUM(IIF(l.Value = 'Present', 1, 0)) AS NoOfDaysPresent,
                                                SUM(IIF(l.Value = 'Absent', 1, 0)) AS NoOfDaysAbsent
                                                FROM Attendance a JOIN Lookup l ON l.lookupID = a.IsPresent
                                                join Person p on p.PersonID=a.StudentID WHERE a.StudentID = @studentid 
                                                AND MONTH(a.Date) = @month GROUP BY a.StudentID,p.FirstName,p.LastName;", con);
                cmd.Parameters.AddWithValue("@studentid", sId);
                cmd.Parameters.AddWithValue("@month", VIewAttendance.month);
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
            return dt;

        }

        public static DataTable ShowResultData()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("SELECT * from Result_View WHERE StudentID = @stId", con);
                cmd.Parameters.AddWithValue("@stId", SignInFrom.UserID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);
                if (dt.Rows.Count == 0)
                {
                    MessageBox.Show(" Result is not uploaded yet .", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
            return dt;

        }

        public static DataTable ShowSelectedResultData(int classId, string session, string term)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand(@"SELECT r.ID,r.StID as StudentID,p.FirstName,r.SubID as SubjectID,s.subjectName,
										      r.TeacheID as TeacherID ,r.Session,r.Term,r.ObtainedMarks FROM Result r 
                                               JOIN ClassStudent cs ON r.StID = cs.StudentID 
										       join Subject s on s.subjectID=r.SubID
										       join Person p on p.PersonID=r.StID
                                                WHERE cs.ClasID = @ClassID 
                                                AND r.Term = @Term 
                                                AND r.Session = @Session", con);
                cmd.Parameters.AddWithValue("@ClassID", classId);
                cmd.Parameters.AddWithValue("@Term", term);
                cmd.Parameters.AddWithValue("@Session", session);
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);
                if (dt.Rows.Count == 0)
                {
                    MessageBox.Show("No Result found.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
            return dt;

        }
        public static DataTable ShowinsertedResultData(int classId, string session, string term)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand(@"SELECT r.ID,r.StID as StudentID,p.FirstName,r.SubID as SubjectID,s.subjectName,
										  r.TeacheID as TeacherID ,r.Session,r.Term,r.ObtainedMarks FROM Result r 
                                           JOIN ClassStudent cs ON r.StID = cs.StudentID 
										   join Subject s on s.subjectID=r.SubID
										   join Person p on p.PersonID=r.StID
                                           WHERE cs.ClasID = @ClassID 
                                           AND r.Term = @Term 
                                           AND r.Session = @Session", con);
                cmd.Parameters.AddWithValue("@ClassID", classId);
                cmd.Parameters.AddWithValue("@Term", term);
                cmd.Parameters.AddWithValue("@Session", session);
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);
                if (dt.Rows.Count == 0)
                {
                    MessageBox.Show("No Result found.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
            return dt;

        }
        public static List<int[]> ClassData(int id)
        {
            List<int[]> dataRows = new List<int[]>();
            string query = @"
        SELECT StudentID, ClassSubject.SubjectID, TeacherID 
        FROM ClassSubject
        JOIN ClassStudent ON ClassSubject.ClassID = ClassStudent.ClasID
        WHERE TeacherID = @id AND Position = 11 AND IsActive = 5;
    ";

            try
            {
                SqlCommand cmd = new SqlCommand(query, con);

                cmd.Parameters.AddWithValue("@id", id);

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int[] ids = new int[3];
                        ids[0] = Convert.ToInt32(reader["StudentID"]);
                        ids[1] = Convert.ToInt32(reader["subID"]);
                        ids[2] = Convert.ToInt32(reader["TteacherID"]);

                        dataRows.Add(ids);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);

            }

            return dataRows;
        }

        public static User Sign_InStudent(User user)
        {
            User credentials = new User();
            try
            {
                SqlCommand cmd = new SqlCommand("\r\nSELECT u.Role, p.PersonID\r\nFROM [User] u\r\nJOIN Person p ON u.ID = p.UserID\r\nWHERE u.UserName = @Username AND u.Password = @password;", con);

                cmd.Parameters.AddWithValue("@Username", user.UserName);
                cmd.Parameters.AddWithValue("@password", user.Password);

                using (SqlDataReader sqlDataReader = cmd.ExecuteReader())
                {

                    if (sqlDataReader.Read())
                    {
                        credentials.Role = int.Parse(sqlDataReader["Role"].ToString());
                        credentials.ID = int.Parse(sqlDataReader["PersonID"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);

            }
            return credentials;

        }

        public static User Sign_InUser(User user)
        {
            User credentials = new User();
            try
            {
                SqlCommand cmd = new SqlCommand("\r\nSELECT u.Role, ID\r\nFROM [User] u WHERE u.UserName = @Username AND u.Password = @password;", con);

                cmd.Parameters.AddWithValue("@Username", user.UserName);
                cmd.Parameters.AddWithValue("@password", user.Password);

                using (SqlDataReader sqlDataReader = cmd.ExecuteReader())
                {

                    if (sqlDataReader.Read())
                    {
                        credentials.Role = int.Parse(sqlDataReader["Role"].ToString());
                        credentials.ID = int.Parse(sqlDataReader["ID"].ToString());
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);

            }
            return credentials;

        }

        public static bool CheckDuplicate(int classId, string term, string session)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(@"SELECT COUNT(*) 
                                                FROM Result r 
                                                JOIN ClassStudent cs ON r.StID = cs.StudentID 
                                                WHERE cs.ClasID = @ClassID 
                                                AND r.Term = @Term 
                                                AND r.Session = @Session", con);
                cmd.Parameters.AddWithValue("@ClassID", classId);
                cmd.Parameters.AddWithValue("@Term", term);
                cmd.Parameters.AddWithValue("@Session", session);

                int count = (int)cmd.ExecuteScalar();
                return count > 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error checking duplicate: " + ex.Message);
                return false;
            }
        }


        public static bool IsTeacherAssignedToClass(int classId, int teacherId)
        {
            SqlCommand cmd = new SqlCommand(@"SELECT COUNT(*)
                     FROM ClassSubject
                     WHERE ClaassID = @ClassId 
                     AND TeacherrID = @TeacherId", con);

            try
            {


                cmd.Parameters.AddWithValue("@ClassId", classId);
                cmd.Parameters.AddWithValue("@TeacherId", teacherId);

                int count = (int)cmd.ExecuteScalar();
                return count > 0;

            }
            catch (Exception ex)
            {
                MessageBox.Show("SQL Error occurred: " + ex.Message);
                return false;
            }

        }






        public static int GetSubject(int classId, int teacherId)
        {
            SqlCommand cmd = new SqlCommand(@"SELECT subID
                     FROM ClassSubject
                     WHERE ClaassID = @classId 
                     AND TeacherrID = @teacherId", con);

            try
            {


                cmd.Parameters.AddWithValue("@classId", classId);
                cmd.Parameters.AddWithValue("@teacherId", teacherId);



                object result = cmd.ExecuteScalar();

                if (result != DBNull.Value && result != null)
                    return Convert.ToInt32(result);
                else
                    return -1;


            }
            catch (Exception ex)
            {

                MessageBox.Show("An error occurred: " + ex.Message);
                return -1;
            }

        }


        public static DataTable ShowhomeWorkData()
        {

            SqlCommand cmd = new SqlCommand("SELECT * FROM Homework", con);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;

        }
        public static void InsertHomework(int teacherId, int subjectId, string homeworkDescription)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("INSERT INTO Homework (TeacherID, subjectId, Date, HomeWork, UpdatedOn, UpdatedBy) VALUES (@TeacherId, @subjectId, @Date,@Homework, GETDATE(), @UpdatedBy)", con);


                cmd.Parameters.AddWithValue("@TeacherId", teacherId);
                cmd.Parameters.AddWithValue("@subjectId", subjectId);
                cmd.Parameters.AddWithValue("@Date", DateTime.Now.Date);
                cmd.Parameters.AddWithValue("@Homework", homeworkDescription);
                cmd.Parameters.AddWithValue("@UpdatedBy", teacherId);


                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {

                MessageBox.Show("An error occurred: " + ex.Message);
                Console.WriteLine("An error occurred: " + ex.Message);
            }

        }

        public static void UpdateHomework(string homeworkId, string newHomework)
        {
            using (SqlTransaction transaction = con.BeginTransaction(System.Data.IsolationLevel.Serializable))
            {
                try
                {
                    string updateQuery = "UPDATE Homework SET Homework = @NewHomework , Date = @date,UpdatedOn =@updated WHERE homeworkID = @homeworkId";
                    using (SqlCommand command = new SqlCommand(updateQuery, con, transaction))
                    {
                        command.Parameters.AddWithValue("@NewHomework", newHomework);
                        command.Parameters.AddWithValue("@homeworkId", homeworkId);
                        command.Parameters.AddWithValue("@date", DateTime.Now);
                        command.Parameters.AddWithValue("@updated", DateTime.Now);
                        int rowsAffected = command.ExecuteNonQuery();

                        if (rowsAffected > 0)
                        {
                            transaction.Commit();
                            MessageBox.Show("Homework updated successfully!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            MessageBox.Show("No homework found with the specified ID.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    MessageBox.Show("An error occurred while updating homework: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }


        public static void DeleteHomework(string homeworkId, string homework)
        {

            using (SqlTransaction transaction = con.BeginTransaction(System.Data.IsolationLevel.Serializable))
            {
                try
                {
                    string deleteQuery = "DELETE FROM Homework WHERE homeworkID = @homeworkId OR  HomeWork = @HomeWork";
                    using (SqlCommand command = new SqlCommand(deleteQuery, con, transaction))
                    {
                        command.Parameters.AddWithValue("@homeworkID", homeworkId);
                        command.Parameters.AddWithValue("@Homework", homework);

                        int rowsAffected = command.ExecuteNonQuery();


                        if (rowsAffected > 0)
                        {
                            transaction.Commit();
                            MessageBox.Show("Home deleted successfully!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            MessageBox.Show("No attendance record found with the specified ID.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    MessageBox.Show("An error occurred while deleting homework: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }















        public static int GetStudentIDsByClassID(int classID, int subjectID)
        {
            int studentID = -1;

            try
            {

                SqlCommand cmd = new SqlCommand("SELECT CS.StudentID AS ID FROM ClassStudent CS INNER JOIN ClassSubject CSS ON CS.ClasID = CSS.ClaassID WHERE CS.ClasID = @ClassID AND CSS.subID = @SubjectID", con);

                cmd.Parameters.AddWithValue("@ClassID", classID);
                cmd.Parameters.AddWithValue("@SubjectID", subjectID);

                using (SqlDataReader reader = cmd.ExecuteReader())
                {

                    if (reader.Read())
                    {
                        studentID = Convert.ToInt32(reader["ID"]);
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("An error occurred: " + ex.Message);
            }

            return studentID;
        }

        public static int GetPersonIdFromUser(int userId)
        {
            int id = 0;
            SqlCommand cmd = new SqlCommand("select PersonID from person join [User] on person.UserId = [user].ID where ID = @id;", con);
            cmd.Parameters.AddWithValue("@id", userId);
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    id = reader.GetInt32(0);
                    break;
                }
            }
            return id;
            
            
        }

        public static bool isIncharge(int id)
        {
            bool incharge = false ;
            SqlCommand cmd = new SqlCommand("select count(*) from ClassSubject WHERE TeacherID = @id AND Position = 11;", con);
            cmd.Parameters.AddWithValue("@id", id);
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    int count = reader.GetInt32(0);
                    incharge = count > 0;
                    break;
                }
            }
            return incharge;
        }

        public static Dictionary<string, int[]> GetTeacherSubjects(int id)
        {
            Dictionary<string, int[]> cmbData = new Dictionary<string, int[]>();
            SqlCommand cmd = new SqlCommand("select Class.ClassGrade + ' - ' + Subject.subjectName, Subject.subjectID, Class.classID from ClassSubject Join Class ON ClassSubject.ClassID = Class.classID Join Subject ON Subject.subjectID = ClassSubject.SubjectID where TeacherID = @id;", con);
            cmd.Parameters.AddWithValue("@id", id);
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    int[] IDs = new int[2];
                    string value = reader.GetString(0);
                    IDs[0] = reader.GetInt32(1);
                    IDs[1] = reader.GetInt32(2);

                    cmbData.Add(value, IDs);
                }
            }

            return cmbData;
        }

        public static DataTable GetHomework(int teacherID)
        {
            SqlCommand cmd = new SqlCommand("select Homework.homeworkID , Class.ClassGrade, Subject.subjectName, HomeWork from Homework JOIN Subject ON Homework.subjectId = Subject.subjectID JOIN Class ON Class.classID = Homework.classId where TeacherID = @id AND UpdatedOn = CONVERT(DATE, GETDATE());", con);
            cmd.Parameters.AddWithValue("@id", teacherID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        public static void UpdateHomework(int id , string hw)
        {
            SqlCommand sqlCommand = new SqlCommand("update Homework set HomeWork = @Homework where homeworkID = @id;", con);
            sqlCommand.Parameters.AddWithValue("@Homework", hw);
            sqlCommand.Parameters.AddWithValue("@id", id);
            sqlCommand.ExecuteNonQuery();

        }

        public static DataTable CreateAttendance(int teacherId)
        {
            string query = @"
        SELECT StudentID, TeacherID, ClassID 
        FROM ClassSubject
        JOIN ClassStudent ON ClassSubject.ClassID = ClassStudent.ClasID
        WHERE TeacherID = @id AND Position = 11 AND IsActive = 5;
    ";
            try
            {
                List<Tuple<int, int, int>> dataToInsert = new List<Tuple<int, int, int>>();

                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.Parameters.AddWithValue("@id", teacherId);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int sId = reader.GetInt32(0);
                            int tId = reader.GetInt32(1);
                            int cId = reader.GetInt32(2);
                            dataToInsert.Add(Tuple.Create(sId, tId, cId));
                        }
                    }
                }

                // Now, iterate over the list and insert data into the Attendance table
                foreach (var data in dataToInsert)
                {
                    using (SqlCommand cmd1 = new SqlCommand("INSERT INTO Attendance Values (@sId, CONVERT(DATE, GETDATE()), @cId, @tId, 1);", con))
                    {
                        cmd1.Parameters.AddWithValue("@sId", data.Item1);
                        cmd1.Parameters.AddWithValue("@cId", data.Item3);
                        cmd1.Parameters.AddWithValue("@tId", data.Item2);
                        cmd1.ExecuteNonQuery();
                    }
                }
            }
            catch
            {

            }

            return GetAttendance(teacherId);
        }


        public static DataTable GetAttendance(int teacherId)
        {
            string query = @"
select Attendance.StudentID, Person.FirstName + ' ' + Person.LastName AS Name, Class.ClassGrade, Attendance.Date, Lookup.Value As Status from Attendance
JOIN Person ON Attendance.StudentID = Person.PersonID
JOIN Class ON Class.classID = Attendance.ClassID
JOIN Lookup ON Lookup.lookupID = Attendance.IsPresent
WHERE Attendance.TeacherID = @tId AND Date = CONVERT(DATE, GETDATE())";
            SqlCommand cmd2 = new SqlCommand(query, con);
            cmd2.Parameters.AddWithValue("@tId", teacherId);
            SqlDataAdapter da = new SqlDataAdapter(cmd2);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;

        }

        public static void UpdateAttendance(int status, int sId, DateTime date)
        {
            SqlCommand cmd = new SqlCommand("update Attendance set IsPresent = @status where StudentID = @sId AND Date = @date;", con);
            cmd.Parameters.AddWithValue("@status", status);
            cmd.Parameters.AddWithValue("@sId", sId);
            cmd.Parameters.AddWithValue("@date", date);
            cmd.ExecuteNonQuery();
        }

        public static void CreateResult(int tId)
        {
            string query = @"
select cs1.ClassID, cs2.SubjectID, cs2.TeacherID, cst.StudentID, s.Year from ClassSubject cs1 
join ClassSubject cs2 on cs1.ClassID = cs2.ClassID
join ClassStudent cst on cst.ClasID = cs1.ClassID
join Person p on p.PersonID = cst.StudentID
join Subject s on s.subjectID = cs2.SubjectID
where cs1.TeacherID = @tID AND cs1.Position = 11 AND cs1.IsActive = 5 AND p.Status = 5;
";
            string query2 = @"  INSERT INTO Result (StID, SubId, TeacheID, Session,Term,ObtainedMarks)
  VALUES (@stID, @subId, @TeacheID, @Session, @Term, @ObtainedMarks)";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@tID", tId);

            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    int cId = reader.GetInt32(0);
                    int suId = reader.GetInt32(1);
                    int teId = reader.GetInt32(2);
                    int sId = reader.GetInt32(3);
                    int year = reader.GetInt32(4);
                    
                    for (int i = 16; i < 19; i++)
                    {
                        SqlCommand cmd2 = new SqlCommand(query2, con);
                        cmd2.Parameters.AddWithValue("@stID", sId);
                        cmd2.Parameters.AddWithValue("@subId", suId);
                        cmd2.Parameters.AddWithValue("@TeacheID", teId);
                        cmd2.Parameters.AddWithValue("@Session", year);
                        cmd2.Parameters.AddWithValue("@Term", i);
                        cmd2.Parameters.AddWithValue("@ObtainedMarks", 0);
                        cmd2.ExecuteNonQuery();
                    }
                }
            }
        }

    }

}




