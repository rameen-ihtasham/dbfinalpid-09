﻿using DBFinalProject.BL;
using DBFinalProject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBFinalProject.Forms
{
    public partial class AddSubjectForm : Form
    {
        public AddSubjectForm()
        {
            InitializeComponent();
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            string name = NameTB.Text;        

            try
            {
                int.Parse(oneMarkTB.Text);
                int.Parse(twoMArkTB.Text);
                int.Parse( threeMarkTB.Text);

                int markone = int.Parse(oneMarkTB.Text);
                int marktwo = int.Parse(twoMArkTB.Text);
                int markthree = int.Parse(threeMarkTB.Text);


                if (NameTB.Text != "" && oneMarkTB.Text != "" && twoMArkTB.Text != "" && threeMarkTB.Text != "")
                {
                    bool isPresent = Queries.IsSubjectPresent(name);
                    if (isPresent)
                    {
                        MessageBox.Show("This subject already exists. Assign another Name");
                    }
                    else
                    {
                        Subject subject = new Subject(0, name, DateTime.Today.Year, 5);
                        SubjectMarks sm1 = new SubjectMarks(0, 16, markone);
                        SubjectMarks sm2 = new SubjectMarks(0, 17, marktwo);
                        SubjectMarks sm3 = new SubjectMarks(0, 18, markthree);

                        Queries.AddSubject(subject, sm1, sm2, sm3);

                        AdminMenuForm form = (AdminMenuForm)this.Tag;
                        form.OpenChildForm(new ManageSubjectsForm());
                    }
                }
            }
            catch
            {
                MessageBox.Show("Marks should be in numbers");
            }
           
          


        }
    }
}
