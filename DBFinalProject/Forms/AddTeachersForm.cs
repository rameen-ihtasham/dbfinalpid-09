﻿using DBFinalProject.BL;
using DBFinalProject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace DBFinalProject.Forms
{
    public partial class AddTeachersForm : Form
    {

        public string password;
        public AddTeachersForm()
        {
            InitializeComponent();
            this.SetData();
            
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            if (FirstNameTB.Text != "" && LastNameTB.Text != "" && AdressTB.Text != "")
            {
                try
                {
                    string firstName = this.FirstNameTB.Text;
                    string lastName = this.LastNameTB.Text;
                    string CNIC = Validation.GetValidCNIC(this.CNICTB.Text);
                    string Contact = Validation.GetValidContact(this.ContactTB.Text);
                    string address = this.AdressTB.Text;
                    string email = Validation.GetValidEmail(this.EmailTB.Text);
                    DateTime dob = DateTime.Parse(this.DOB.Text);
                    int designation = (int)this.DesignationCB.SelectedValue;
                    int gender = (int)this.GenderCB.SelectedValue;
                    string username = "@" + firstName + lastName;

                    TeacherPassword tp = new TeacherPassword(username);
                    tp.FormClosing += SetPassword;
                    tp.ShowDialog();


                    User u = new User(0, username, password, 7);
                    Person p = new Person(0, firstName, lastName, Contact, email, dob, address, CNIC, gender, 5, System.DateTime.Today.Year - dob.Year, 0);
                    Teacher t = new Teacher(0, designation);

                    Queries.AddTeacher(u, p, t);


                    AdminMenuForm form = (AdminMenuForm)this.Tag;
                    form.OpenChildForm(new ManageTeachersForm());
                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Fill the Text Boxes");
            }


        }

        private void SetPassword(object sender, FormClosingEventArgs e)
        {
            TeacherPassword testForm = sender as TeacherPassword;
            this.password = testForm.password;
            
        }

        public void SetData()
        {
            GenderCB.DisplayMember = "Value";
            GenderCB.ValueMember = "Key";
            this.GenderCB.DataSource = new BindingSource(Dictionaries.genders, null);
            DesignationCB.DisplayMember = "Value";
            DesignationCB.ValueMember = "Key";
            this.DesignationCB.DataSource = new BindingSource(Dictionaries.desginations, null);
        }

        private void AddBtn_Click_1(object sender, EventArgs e)
        {
            if (FirstNameTB.Text != "" && LastNameTB.Text != "" && AdressTB.Text != "")
            {
                try
                {
                    string firstName = this.FirstNameTB.Text;
                    string lastName = this.LastNameTB.Text;
                    string CNIC = Validation.GetValidCNIC(this.CNICTB.Text);
                    string Contact = Validation.GetValidContact(this.ContactTB.Text);
                    string address = this.AdressTB.Text;
                    string email = Validation.GetValidEmail(this.EmailTB.Text);
                    DateTime dob = DateTime.Parse(this.DOB.Text);
                    int designation = (int)this.DesignationCB.SelectedValue;
                    int gender = (int)this.GenderCB.SelectedValue;
                    string username = "@" + firstName + lastName;

                    TeacherPassword tp = new TeacherPassword(username);
                    tp.FormClosing += SetPassword;
                    tp.ShowDialog();


                    User u = new User(0, username, password, 7);
                    Person p = new Person(0, firstName, lastName, Contact, email, dob, address, CNIC, gender, 5, System.DateTime.Today.Year - dob.Year, 0);
                    Teacher t = new Teacher(0, designation);

                    Queries.AddTeacher(u, p, t);


                    AdminMenuForm form = (AdminMenuForm)this.Tag;
                    form.OpenChildForm(new ManageTeachersForm());

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Fill the Text Boxes");
            }


        }



    }
}
