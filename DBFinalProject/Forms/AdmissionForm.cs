﻿using DBFinalProject.BL;
using DBFinalProject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics.Eventing.Reader;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace DBFinalProject.Forms
{
    public partial class AdmissionForm : Form
    {
        int status;
        public int RClass;
        public Person student;
        public Student stu;
        public User user;
        public ClassStudent studentClass;
        public Class clas;


        public AdmissionForm()
        {

            InitializeComponent();
         
            cn();
          
            if (status == 3)
            { GenderCB.Text = "Male"; }
            else if (status == 4)
            { GenderCB.Text = "Female"; }


        }
        private bool isvalidate()
        {
            if (string.IsNullOrEmpty(FirstNameTB.Text) ||
              string.IsNullOrEmpty(LastNameTB.Text) ||
              string.IsNullOrEmpty(ContactTB.Text) ||
              string.IsNullOrEmpty(EmailTB.Text) ||
              string.IsNullOrEmpty(DOB.Text) ||
              string.IsNullOrEmpty(AdressTB.Text) ||
              string.IsNullOrEmpty(CNICTB.Text) ||
           
              string.IsNullOrEmpty(RclassCB.Text))
            {
                MessageBox.Show("Please fill in all fields.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                cleararea();
                return false;
            }

            if (!Regex.IsMatch(CNICTB.Text.Trim(), @"^\d{5}-\d{7}-\d{1}$"))
            {
                MessageBox.Show("CNIC format is incorrect. Please use format 42301-4438557-0.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                CNICTB.Clear();
                return false;
            }

            if (!EmailTB.Text.Contains("@"))
            {
                MessageBox.Show("Email format is incorrect. Please include '@' in the email address.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
               EmailTB. Clear();
                return false;
            }

            if (ContactTB.Text.Length != 11 || !ContactTB.Text.All(char.IsDigit))
            {
                MessageBox.Show("Contact number must have 11 digits.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ContactTB.Clear();
                return false;
            }
            return true;
        }
        public  void GetInfoFromTextboxes()
        {
           
            int id_check = 0;
            if (GenderCB.Text == "Male")
            {
                id_check = 3;
            }
            else
            {
                id_check = 4;
            }
           
            
                string firstName = FirstNameTB.Text;
                string lastName = LastNameTB.Text;

                string contact = ContactTB.Text;
                string email = EmailTB.Text;
                RClass = int.Parse(RclassCB.Text.Trim());
                DateTime dob1 = DateTime.Parse(DOB.Text);
                DateTime currentDate = DateTime.Today;
                int age = currentDate.Year - dob1.Year;
                if (currentDate.Month < dob1.Month || (currentDate.Month == dob1.Month && currentDate.Day < dob1.Day))
                {
                    age = age - 1;
                }
                string address = AdressTB.Text;

                string cnic = CNICTB.Text;

                this.user = new User(("@"+firstName +  lastName), StudentPassword.passwords, 9);
                this.student = new Person(firstName, lastName, contact, email, dob1, address, cnic, id_check, 5, age);
                this.studentClass = new ClassStudent(RClass);

            

        }

        public void cleararea()
        {
            FirstNameTB.Clear();
            LastNameTB.Clear();
         
            ContactTB.Clear();
            EmailTB.Clear() ;
            GenderCB.SelectedIndex = -1;
            AdressTB.Clear();
            RclassCB.SelectedIndex = -1;
            CNICTB.Clear();
        }
        public void InsertStudentData()
        {
            Queries.InsertStudentUser(this.user, this.stu, this.student,RClass);
        }

        public  void cn(Person student = null,Student stu=null,Class clas=null)
        {
            this.student=student;
            if (!(this.student is null))
            {
              FirstNameTB.Text = student.FirstName;
                LastNameTB.Text = student.LastName;
             
                ContactTB.Text = student.Contact;
                EmailTB.Text = student.Email;
                GenderCB.Text = student.Gender.ToString();
                DOB.Text = student.DOB.ToLongDateString();
                AdressTB.Text = student.Address;
                RclassCB.Text = clas.ClassGrade.ToString();
                CNICTB.Text = student.CNIC.ToString();



            }
        }
        private void guna2HtmlLabel10_Click(object sender, EventArgs e)
        {

        }

        private void AddBtn_Click(object sender, EventArgs e)
        {

            if (!isvalidate())
                return;
            StudentPassword passwordForm = new StudentPassword(student, user, studentClass, stu);
            passwordForm.ShowDialog();


        }

        private void guna2ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void GenderCB_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
