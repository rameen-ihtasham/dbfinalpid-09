﻿using DBFinalProject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBFinalProject.Forms
{
    public partial class AlternateTeacherForm : Form
    {
        public int alternateID = -1;
        public int teacherID;
        public AlternateTeacherForm(int teacherId)
        {
            InitializeComponent();
            this.teacherID = teacherId;
            this.SetData();
        }

        public void SetData()
        {
            alternateCB.DisplayMember = "Value";
            alternateCB.ValueMember = "Key";
            this.alternateCB.DataSource = new BindingSource( Queries.GetAlternateTeachersDictionary(teacherID) , null);
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            int newTeacher = (int)alternateCB.SelectedValue;
            Queries.AssignAlternateTeacher(this.teacherID, newTeacher);
            this.Close();
        }
    }
}
