﻿using DBFinalProject.BL;
using DBFinalProject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Linq;

namespace DBFinalProject.Forms
{
    public partial class AssignSubjectForm : Form
    {
        public Dictionary<int, string> classes;
        public Dictionary<int, string> teachers;
        public Dictionary<int, string> subjects;
        public Dictionary<int, string> position;

        public AssignSubjectForm()
        {
            InitializeComponent();                       
            SetData();
            loadGrid();
            classCB.SelectedIndex = 0;
            position = new Dictionary<int, string>()
            {
                {11,"Incharge" },
                {10,"Lecturer" }
            };
        }

        public void SetData()
        {

            classCB.DisplayMember = "Value";
            classCB.ValueMember = "Key";
            classCB.DataSource = new BindingSource(Queries.GetClassesDictionary(), null);
            classes = Queries.GetClassesDictionary();

            positionCB.DisplayMember = "Value";
            positionCB.ValueMember = "Key";
            positionCB.DataSource = new BindingSource(position, null);          

            subjectCB.DisplayMember = "Value";
            subjectCB.ValueMember = "Key";
            subjectCB.DataSource = new BindingSource(Queries.GetSubjectDictionary(), null);
            subjects = Queries.GetSubjectDictionary();

            teacherCB.DisplayMember= "Value";
            teacherCB.ValueMember = "Key";
            teacherCB.DataSource = new BindingSource(Queries.GetTeacherDictionary(), null);
            teachers = Queries.GetTeacherDictionary();

            classCB_SelectedIndexChanged(null, null);
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            try
            { 
                if (teacherCB.SelectedValue != null && subjectCB.SelectedValue != null && classCB.SelectedValue != null && positionCB.SelectedValue != null)
                
                {
                    int teacherId = (int)teacherCB.SelectedValue;
                int subjectId = (int)subjectCB.SelectedValue;
                int classId = (int)classCB.SelectedValue;
                int position = (int)positionCB.SelectedValue;



                    ClassSubject classSubject = new ClassSubject(classId, subjectId, teacherId, position, 5);

                    Queries.AddClassSubject(classSubject);

                    loadGrid();
                    classCB.Enabled = true;
                    subjectCB.Enabled = true;
                    classCB_SelectedIndexChanged(null, null);

                }
                else
                {
                    MessageBox.Show("An Item Not Selected");
                }
            }
            catch
            {
                MessageBox.Show("Select All Items");
            }
        }

        public void loadGrid()
        {
            this.DataTable.DataSource = Queries.GetClassSubjectsData();
        }

        private void guna2Button1_Click(object sender, EventArgs e) 
        {
            if (DataTable.SelectedRows[0].Cells[0].Value != null)
            {
                int id = (int)classCB.SelectedValue;
                int classGradeId = classes.FirstOrDefault(x => x.Value == (string)DataTable.SelectedRows[0].Cells[0].Value).Key;
                int subjectId = subjects.FirstOrDefault(x => x.Value == (string)DataTable.SelectedRows[0].Cells[1].Value).Key;
                int teacherId = teachers.FirstOrDefault(x => x.Value == (string)DataTable.SelectedRows[0].Cells[2].Value).Key;
                int positionId = Dictionaries.positions.FirstOrDefault(x => x.Value == (string)DataTable.SelectedRows[0].Cells[3].Value).Key;
                subjectCB.DataSource = new BindingSource(Queries.GetSubjectDictionary(), null);
                classCB.SelectedIndexChanged -= classCB_SelectedIndexChanged;
                classCB.SelectedValue = classGradeId;
                classCB.SelectedIndexChanged += classCB_SelectedIndexChanged;
                classCB.Enabled = false;
                subjectCB.DisplayMember = "Value";
                subjectCB.ValueMember = "Key";
                subjectCB.SelectedValue = subjectId;
                subjectCB.Enabled = false;
                teacherCB.SelectedValue = teacherId;

                if (Queries.ClassHasIncharge(id))
                {
                    if (positionCB.SelectedText == "Lecturer")
                    {
                        position = new Dictionary<int, string>()
                    {
                        {10,"Lecturer" }
                    };
                    }
                }
                else
                {
                    position = new Dictionary<int, string>()
                {
                    {11,"Incharge" },
                    {10,"Lecturer" }
                };
                }
                positionCB.DataSource = new BindingSource(position, null);

                positionCB.SelectedValue = positionId;
            }
            else
            {
                MessageBox.Show("Select a class subject to edit");
            }
        }

        private void classCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            int id = (int)classCB.SelectedValue;
            Dictionary<int, string> subjects = Queries.GetSubjectsForClass(id);
            if (subjects.Count > 0)
            {
                subjectCB.DisplayMember = "Value";
                subjectCB.ValueMember = "Key";
                subjectCB.DataSource = new BindingSource(subjects, null);
            }
            else
            {
                subjectCB.DataSource = null;
            }

            if (Queries.ClassHasIncharge(id))
            {
                position = new Dictionary<int, string>()
                {
                    {10,"Lecturer" }
                };
            }
            else
            {
                position = new Dictionary<int, string>()
                {
                    {11,"Incharge" },
                    {10,"Lecturer" }
                };
            }

            positionCB.DataSource = new BindingSource(position, null);
        }
    }
}
