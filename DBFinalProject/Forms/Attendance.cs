﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using DBFinalProject.DataAccess;
using System.Threading.Tasks;
using System.Windows.Forms;
using DBFinalProject.BL;
using System.Data.SqlClient;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using Guna.UI2.WinForms;
using static System.Collections.Specialized.BitVector32;
using static System.ComponentModel.Design.ObjectSelectorEditor;
using System.Reflection;

namespace DBFinalProject.Forms
{
    public partial class Attendance : Form
    {
        public Attendance(attendanceMark a)
        {
            InitializeComponent();
        }

        private void Attendance_Load(object sender, EventArgs e)
        {
            TeacherMenuForm tmf = this.Tag as TeacherMenuForm;
            guna2DataGridView2.DataSource = Queries.CreateAttendance(tmf.teacherId);
        }

        public void UpdateDGV()
        {
            TeacherMenuForm tmf = this.Tag as TeacherMenuForm;
            guna2DataGridView2.DataSource = Queries.GetAttendance(tmf.teacherId);
        }

        private void AbsentBtn_Click(object sender, EventArgs e)
        {
            int id = (int)guna2DataGridView2.SelectedRows[0].Cells[0].Value;
            DateTime date = (DateTime)guna2DataGridView2.SelectedRows[0].Cells[3].Value;
            string status = guna2DataGridView2.SelectedRows[0].Cells[4].Value.ToString();
            int updatedStatusId = status == "Present" ? 2 : 1;
            Queries.UpdateAttendance(updatedStatusId, id, date);
            UpdateDGV();
        }
    }
}

