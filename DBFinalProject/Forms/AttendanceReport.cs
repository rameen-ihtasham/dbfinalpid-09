﻿using DBFinalProject.CrystalReports;
using DBFinalProject.DataAccess;
using Guna.UI2.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBFinalProject.Forms
{
    public partial class AttendanceReport : Form
    {
        public static int mlId = 0;
        public AttendanceReport()
        {
            InitializeComponent();
            guna2DataGridView1.DataSource = Queries.ShowStudentInfo();
        }

        private void guna2DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        int indexrow;
        private void guna2DataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            indexrow = e.RowIndex;
            DataGridViewRow row = guna2DataGridView1.Rows[indexrow];
            string idstring = row.Cells[0].Value.ToString();
            string ml = row.Cells[0].Value.ToString();
            if (string.IsNullOrWhiteSpace(idstring))
            {
                MessageBox.Show("Please select a valid column");
                return;
            }
            guna2TextBox1.Text = ml;
            mlId = int.Parse(idstring);
        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(guna2TextBox1?.Text))
            {

                MessageBox.Show("Please Select Student ID to generate Attendance Report");
                return;
            }
            else
            {
                Form f = new generateAttendanceReport();
                f.Show();
                guna2TextBox1.Clear();
            }
        }
    }
}
