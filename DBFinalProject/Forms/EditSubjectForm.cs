﻿using DBFinalProject.BL;
using DBFinalProject.DataAccess;
using Guna.UI2.WinForms.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBFinalProject.Forms
{
    public partial class EditSubjectForm : Form
    {
        public int subId;
        public string name;
        public int marksOne;
        public int marksTwo;
        public int marksThree;
        public EditSubjectForm(int id , string name, int marks1 , int marks2 , int marks3)
        {
            InitializeComponent();
            this.subId = id;
            this.name = name;
            this.marksOne = marks1;
            this.marksTwo = marks2;
            this.marksThree = marks3;
            this.fillData();

        }
        public void fillData()
        {
            this.NameTB.Text = this.name;
            this.oneMarkTB.Text = this.marksOne.ToString();
            this.twoMArkTB.Text = this.marksTwo.ToString();
            this.threeMarkTB.Text = this.marksThree.ToString();
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.name = this.NameTB.Text;
                this.marksOne = int.Parse(this.oneMarkTB.Text);
                this.marksTwo = int.Parse(this.twoMArkTB.Text);
                this.marksThree = int.Parse(this.threeMarkTB.Text);

                if (NameTB.Text != "" && oneMarkTB.Text != "" && twoMArkTB.Text != "" && threeMarkTB.Text != "")
                {
                    bool isPresent = Queries.IsSelfSubjectPresent(name, this.subId);
                    if (isPresent)
                    {
                        MessageBox.Show("This subject already exists. Assign another Name");
                    }
                    else
                    {
                        SubjectMarks sm1 = new SubjectMarks(this.subId, 16, marksOne);
                        SubjectMarks sm2 = new SubjectMarks(this.subId, 17, marksTwo);
                        SubjectMarks sm3 = new SubjectMarks(this.subId, 18, marksThree);
                        Queries.UpdateSubject(this.subId, this.name, sm1, sm2, sm3);

                        AdminMenuForm form = (AdminMenuForm)this.Tag;
                        form.OpenChildForm(new ManageSubjectsForm());
                    }
                }
            }
            catch
            {
                MessageBox.Show("Invalid Value");
            }
        }
    }
}
