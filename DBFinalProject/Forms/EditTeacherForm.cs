﻿using DBFinalProject.BL;
using DBFinalProject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBFinalProject.Forms
{
    public partial class EditTeacherForm : Form
    {
        public Person person;
        public Teacher teacher;
        public EditTeacherForm(Person person , Teacher teacher)
        {
            InitializeComponent();
            this.person = person;   
            this.teacher = teacher;
            this.setData();
            this.fillText();
        }

        public void setData()
        {
            GenderCB.DisplayMember = "Value";
            GenderCB.ValueMember = "Key";
            this.GenderCB.DataSource = new BindingSource(Dictionaries.genders, null);
            DesignationCB.DisplayMember = "Value";
            DesignationCB.ValueMember = "Key";
            this.DesignationCB.DataSource = new BindingSource(Dictionaries.desginations, null);
        }

        public void fillText()
        {
            this.FirstNameTB.Text = person.FirstName;
            this.LastNameTB.Text = person.LastName;
            this.CNICTB.Text = person.CNIC;
            this.ContactTB.Text = person.Contact;
            this.GenderCB.SelectedValue = person.Gender;
            this.EmailTB.Text = person.Email;
            this.AdressTB.Text = person.Address;
            this.DOB.Text = person.DOB.ToString();
            this.DesignationCB.SelectedValue = teacher.Designation;
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            if (FirstNameTB.Text != "" && LastNameTB.Text != "" && AdressTB.Text != "")
            {
                try
                {
                    person.FirstName = this.FirstNameTB.Text;
                    person.LastName = this.LastNameTB.Text;
                    person.CNIC = Validation.GetValidCNIC(this.CNICTB.Text);
                    person.Contact = Validation.GetValidContact(this.ContactTB.Text);
                    person.Gender = (int)this.GenderCB.SelectedValue;
                    person.Email = Validation.GetValidEmail(this.EmailTB.Text);
                    person.Address = this.AdressTB.Text;
                    person.DOB = DateTime.Parse(this.DOB.Text);
                    teacher.Designation = (int)this.DesignationCB.SelectedValue;

                    Queries.UpdateTeacher(teacher , person);

                    AdminMenuForm form = (AdminMenuForm)this.Tag;
                    form.OpenChildForm(new ManageTeachersForm());
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
    }

}
