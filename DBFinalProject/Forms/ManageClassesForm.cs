﻿using DBFinalProject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBFinalProject.Forms
{
    public partial class ManageClassesForm : Form
    {
        public ManageClassesForm()
        {
            InitializeComponent();
            this.loadGrid();
            
        }
        public void loadGrid()
        {
            this.DataTable.DataSource = Queries.GetClassesData();
            this.ClassCB.DataSource = Queries.GetAvailableClasses();
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            int classNumber = (int)ClassCB.SelectedValue;
            Queries.AddClass(classNumber);

            loadGrid();
        }
    }
}
