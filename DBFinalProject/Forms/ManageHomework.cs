﻿using DBFinalProject.BL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DBFinalProject.DataAccess;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using System.Web.UI;
using Guna.UI2.WinForms;
using System.Web.UI.WebControls;
using Guna.UI2.WinForms.Suite;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;
namespace DBFinalProject.Forms
{
    public partial class ManageHomework : Form
    {
        public int classId;
        public int teacherId;
        public string description;

        public static SqlConnection con = Configuration.getInstance().getConnection();

        public ManageHomework(int teacherId)
        {
            InitializeComponent();
            this.teacherId = teacherId;

            ClassCB.DisplayMember = "Key";
            ClassCB.ValueMember = "Value";
            ClassCB.DataSource = new BindingSource(Queries.GetTeacherSubjects(teacherId), null);
            RefreshDGV();

            
        }
      
        private void RefreshDGV()
        {
            this.guna2DataGridView2.DataSource = Queries.GetHomework(teacherId);
        }
       

        private void cleardata()
        {
            richTextBox1.Text = "";
            ClassCB.Text = "";
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private bool ContainsOnlyInteger(string input)
        {
            // Regular expression to check if input contains only integer values
            return Regex.IsMatch(input, @"^\d+$");
        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            // Check if both combo box and description are empty
            if (string.IsNullOrEmpty(ClassCB.Text) || string.IsNullOrEmpty(richTextBox1.Text))
            {
                MessageBox.Show("Please fill all fields.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return; // Exit the method since validation failed
            }
            if (ContainsOnlyInteger(richTextBox1.Text))
            {
                MessageBox.Show("Description should contain at least one non-integer character.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return; // Exit the method since validation failed
            }
            string homeworkdescription = richTextBox1.Text;
            int[] ids = (int[])ClassCB.SelectedValue;
            int subId = ids[0];
            int classId = ids[1];

                // Call the stored procedure to insert homework
                try
                {
                    using (SqlCommand cmd = new SqlCommand("stp_addHomework", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@SubjectId", subId);
                        cmd.Parameters.AddWithValue("@TeacherID", teacherId);
                        cmd.Parameters.AddWithValue("@Homework", homeworkdescription);
                        cmd.Parameters.AddWithValue("@UpdatedBy", teacherId);
                        cmd.Parameters.AddWithValue("@ClassID", classId);

                        cmd.ExecuteNonQuery();
                        MessageBox.Show("Homework added successfully!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("An error occurred while adding homework: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            cleardata();
            RefreshDGV();
        }




        private void guna2Button3_Click_1(object sender, EventArgs e)
        {
        }

        private void guna2Button2_Click(object sender, EventArgs e)
        {
            if (guna2DataGridView2.SelectedRows[0].Cells[0].Value != null)
            {
                int id = (int)guna2DataGridView2.SelectedRows[0].Cells[0].Value;
                string newhomework = richTextBox1.Text;
                Queries.UpdateHomework(id, newhomework);
                RefreshDGV();
            }
            else
            {
                MessageBox.Show("Select homework first");
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}


