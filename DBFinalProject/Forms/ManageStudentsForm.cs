﻿using DBFinalProject.BL;
using DBFinalProject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBFinalProject.Forms
{
    public partial class ManageStudentsForm : Form
    {
        public ManageStudentsForm()
        {
            InitializeComponent();
            this.loadGrid();
            
        }

        public void loadGrid()
        {
            this.DataTable.DataSource = Queries.GetStudentsData();
        }

        private void DeleteBtn_Click(object sender, EventArgs e)
        {
            if (DataTable.SelectedRows[0].Cells[0].Value != null)
            {

                if (this.DataTable.SelectedRows.Count <= 0)
                {
                    MessageBox.Show("Select any row first");
                }
                else
                {
                    int id = (int)this.DataTable.SelectedRows[0].Cells[0].Value;
                    Queries.DeleteStudent(id);
                    this.loadGrid();
                }
            }
            else
            {
                MessageBox.Show("Select a Student");
            }
        }

        private void EditBtn_Click(object sender, EventArgs e)
        {
            if (DataTable.SelectedRows[0].Cells[0].Value != null)
            {


                int id = (int)this.DataTable.SelectedRows[0].Cells[0].Value;
                Person p = Queries.GetStudentAttributes(id);
                AdminMenuForm form = (AdminMenuForm)this.Tag;
                form.OpenChildForm(new EditStudentForm(p));
            }
            else
            {
                MessageBox.Show("Select a Student");
            }
        }

        private void DataTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
