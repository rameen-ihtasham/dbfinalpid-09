﻿using DBFinalProject.BL;
using DBFinalProject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBFinalProject.Forms
{
    public partial class ManageSubjectsForm : Form
    {
        public ManageSubjectsForm()
        {
            InitializeComponent();
            this.loadGrid();

        }

        private void guna2Panel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void guna2HtmlLabel1_Click(object sender, EventArgs e)
        {

        }

        private void guna2TextBox1_TextChanged(object sender, EventArgs e)
        {
            
        }
        public void loadGrid()
        {
            this.DataTable.DataSource = Queries.GetSubjectsData();
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            AdminMenuForm form = (AdminMenuForm)this.Tag;
            form.OpenChildForm(new AddSubjectForm());
        }

        private void DeleteBtn_Click(object sender, EventArgs e)
        {
            if (DataTable.SelectedRows[0].Cells[0].Value != null)
            {
                int id = (int)DataTable.SelectedRows[0].Cells[0].Value;
                Queries.DeleteSubject(id);
                loadGrid();
            }
            else
            {
                MessageBox.Show("Select a subject first");
            }

        }

        private void Editbtn_Click(object sender, EventArgs e)
        {
            if (DataTable.SelectedRows[0].Cells[0].Value != null)
            {
                int id = (int)DataTable.SelectedRows[0].Cells[0].Value;
                string name = DataTable.SelectedRows[0].Cells[1].Value.ToString();
                int termOneMarks = Queries.getSubjectMarks(16, id);
                int termTwoMarks = Queries.getSubjectMarks(17, id);
                int termThreeMarks = Queries.getSubjectMarks(18, id);


                AdminMenuForm form = (AdminMenuForm)this.Tag;
                form.OpenChildForm(new EditSubjectForm(id, name, termOneMarks, termTwoMarks, termThreeMarks));
            }
            else
            {
                MessageBox.Show("Select a subject first");
            }           

        }
    }
}
