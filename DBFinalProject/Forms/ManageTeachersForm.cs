﻿using DBFinalProject.BL;
using DBFinalProject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBFinalProject.Forms
{
    public partial class ManageTeachersForm : Form
    {

        public int alternateTeacher;
        public ManageTeachersForm()
        {
            InitializeComponent();
            loadGrid();
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            AdminMenuForm form = (AdminMenuForm)this.Tag;
            form.OpenChildForm(new AddTeachersForm());
        }

        public void loadGrid()
        {
            this.DataTable.DataSource = Queries.GetTeachersData();
        }

        private void EditBtn_Click(object sender, EventArgs e)
        {
            if (DataTable.SelectedRows[0].Cells[0].Value != null)
            {
                int id = (int)DataTable.SelectedRows[0].Cells[0].Value;

                Person p = Queries.GetPersonAttributes(id);
                Teacher t = Queries.GetTeacherAttributes(id);

                AdminMenuForm form = (AdminMenuForm)this.Tag;
                form.OpenChildForm(new EditTeacherForm(p, t));
            }
            else {
                MessageBox.Show("Select teacher first");
            }


        }

        private void DeleteBtn_Click(object sender, EventArgs e)
        {
            
            if (DataTable.SelectedRows[0].Cells[0].Value != null)
            {
                int id = (int)DataTable.SelectedRows[0].Cells[0].Value;
                bool isClassTeacher = Queries.IsClassTeacher(id);
                if (isClassTeacher)
                {
                    AlternateTeacherForm tf = new AlternateTeacherForm(id);
                    tf.FormClosing += this.SetAlternate;
                    tf.ShowDialog();
                    if (this.alternateTeacher != -1)
                    {
                        Queries.DeleteTeacher(id);
                    }
                }
                else
                {
                    Queries.DeleteTeacher(id);
                    this.loadGrid();
                }
            }
            else
            {
                MessageBox.Show("Select teacher first");
            }

        }
        private void SetAlternate(object sender, FormClosingEventArgs e)
        {
            AlternateTeacherForm testForm = sender as AlternateTeacherForm;
            this.alternateTeacher = testForm.alternateID ;

        }
    }
}
