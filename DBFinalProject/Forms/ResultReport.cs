﻿using DBFinalProject.CrystalReports;
using DBFinalProject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBFinalProject.Forms
{
    public partial class ResultReport : Form
    {
        public static int mlId = 0;
      
        public ResultReport()
        {
            InitializeComponent();
            UplaodResult.DataSource = Queries.ShowStudentInfo();
        }

       
        private void UplaodResult_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        int indexrow;
        private void UplaodResult_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            indexrow = e.RowIndex;
            DataGridViewRow row = UplaodResult.Rows[indexrow];
            string idstring = row.Cells[0].Value.ToString();
            string ml = row.Cells[0].Value.ToString();
            if (string.IsNullOrWhiteSpace(idstring))
            {
                MessageBox.Show("Please select a valid column");
                return;
            }
            guna2TextBox1.Text = ml;
            mlId = int.Parse(idstring);
        }

        private void UplaodResult_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {


        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(guna2TextBox1?.Text))
            {
                MessageBox.Show("Please Select Student ID to generate Result");
                return;
            }
            else
            {
                Form f = new GenerateResultReport();
                f.Show();
                guna2TextBox1.Clear();
            }
        }
    }
}
