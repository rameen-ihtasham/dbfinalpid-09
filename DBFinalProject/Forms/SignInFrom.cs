﻿using DBFinalProject.BL;
using DBFinalProject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBFinalProject.Forms
{
    public partial class SignInFrom : Form
    {
        public Form activeForm = null;
        public User user;
        public static int UserID;

     
        public SignInFrom()
        {

            InitializeComponent();
            cn();
        }
        public void OpenChildForm(Form childForm)
        {
            activeForm?.Close();
            activeForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            MainWindow.Controls.Add(childForm);
            this.Tag = childForm;
          
            childForm.Tag = this;
            childForm.BringToFront();
            childForm.Show();
        }

    
            

           

        public void cn(User user=null)
        {
            
                this.user = user;
                if (!(this.user is null))
                {

                    UserNameTextBox.Text = user.UserName;
                    PasswordTextBox.Text = user.Password;

                }

        
        }
       

        private void LoginBtn_Click(object sender, EventArgs e)
        {
            if (!ValidateInput())
                return;

            string username = UserNameTextBox.Text;
            string password = PasswordTextBox.Text;
            this.user = new User(username, password);
            User credentials=Queries.Sign_InUser(user);
            int userRole = credentials.Role;
            UserID = credentials.ID;
           

            if (userRole == 9)
            {
                MainWindowForm form = (MainWindowForm)this.Tag;
               form. OpenChildForm(new StudentMenuForm(credentials));
            }
            else if (userRole == 7)
            {
                MainWindowForm form = (MainWindowForm)this.Tag;
                form.OpenChildForm(new TeacherMenuForm(credentials));
            }

            else if (userRole == 8)
            {

                MainWindowForm form = (MainWindowForm)this.Tag;
               form. OpenChildForm(new AdminMenuForm());
            }
            else
            {
                MessageBox.Show("Incorrect credentials. Please enter the correct username and password.");
                cleardata();
            }

        }
        private bool ValidateInput()
        {
            if (string.IsNullOrWhiteSpace(UserNameTextBox.Text) && string.IsNullOrWhiteSpace(PasswordTextBox.Text))
            {
                MessageBox.Show("Please enter both username and password.");
                return false;
            }
            if (string.IsNullOrWhiteSpace(UserNameTextBox.Text))
            {
                MessageBox.Show("Please enter username  ");
                return false;
            }
            if (string.IsNullOrWhiteSpace(PasswordTextBox.Text))
            {
                MessageBox.Show("Please enter  password.");
                return false;
            }
            return true;
        }
        public void cleardata()
        {
            UserNameTextBox.Clear();
            PasswordTextBox.Clear();
            UserNameTextBox.Focus();

        }
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Form f=new AdmissionForm();
            f.Show();
           // MainWindowForm form = (MainWindowForm)this.Tag;
           // form.Hide();
        }

        private void PasswordTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void guna2CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (guna2CheckBox1.Checked)
            {
                PasswordTextBox.PasswordChar = '\0'; // Show password characters
            }
            else
            {
                PasswordTextBox.PasswordChar = '*'; // Hide password characters
            }
        }
    }
}
