﻿using DBFinalProject.BL;
using DBFinalProject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBFinalProject.Forms
{
    public partial class StudentPassword : Form
    {
        public static string passwords;
       
        public Person student;
        public User user;
        public ClassStudent studentClass;
        public Student stu;

        public AdmissionForm admissionForm;
        public StudentPassword(Person student = null,User user = null,ClassStudent classStudent=null,Student stu=null)
        {
            
            this.student = student;
            this.user = user;
            this.studentClass = classStudent;
            this.stu = stu;
            InitializeComponent();
           
            this.admissionForm = (AdmissionForm)Application.OpenForms["AdmissionForm"];
           


        }
        
        private void guna2Button1_Click(object sender, EventArgs e)
        {
           
            try
            {
                passwords = passwordTB.Text;
                
                admissionForm.GetInfoFromTextboxes();
                if (string.IsNullOrEmpty( passwords))
                    {

                    MessageBox.Show("Please enter Password to register .", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    passwordTB.Clear();
                    return;
                }
                if (!Regex.IsMatch(passwords, @"\d+"))
                {
                    MessageBox.Show("Password must contain characters  and at least one digit.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    passwordTB.Clear();
                    return;
                }

                if (passwords.Length > 15)
                {
                    MessageBox.Show("Password length must be less than or equal to 15 characters.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    passwordTB.Clear();
                    return;
                }
                admissionForm.InsertStudentData ();
              
               
                admissionForm. Hide();

               

            }
            catch (Exception ex)
            {
                MessageBox.Show($"An error occurred: {ex.Message}");
                admissionForm?.cleararea();


            }

            this.Close();
        }

        private void passwordTB_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
