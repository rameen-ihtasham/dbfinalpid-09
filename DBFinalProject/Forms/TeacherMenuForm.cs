﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DBFinalProject.BL;
using DBFinalProject.DataAccess;
namespace DBFinalProject.Forms
{
    public partial class TeacherMenuForm : Form
    {
        public Form activeForm = null;
        public Action loadGrid;
        public int teacherId;
        public TeacherMenuForm(User user)
        {
            InitializeComponent();
            teacherId = Queries.GetPersonIdFromUser(user.ID);
            if (!Queries.isIncharge(teacherId))
            {
                guna2Button1.Visible = false;
                guna2Button2.Visible = false;
            }
             
        }

        public void OpenChildForm(Form childForm)
        {
            activeForm?.Close();
            activeForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            guna2Panel3.Controls.Add(childForm);
            childForm.Tag = this;
            this.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
        }
        private void guna2Button1_Click(object sender, EventArgs e)
        {

            attendanceMark h = new attendanceMark();
            OpenChildForm(new Attendance(h));



        }

        private void guna2Button2_Click(object sender, EventArgs e)
        {
            MarksUpload h = new MarksUpload();
            OpenChildForm(new UploadResult(h));
        }

        private void guna2Button3_Click(object sender, EventArgs e)
        {
            OpenChildForm(new ManageHomework(teacherId));
        }

        private void guna2Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void guna2Panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void guna2Button4_Click(object sender, EventArgs e)
        {
            MainWindowForm form = (MainWindowForm)this.Parent;
            form.OpenChildForm(new SignInFrom());
        }
    }
}
