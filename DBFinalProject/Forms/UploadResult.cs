﻿using DBFinalProject.BL;
using DBFinalProject.DataAccess;
using DBFinalProject.Forms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;



namespace DBFinalProject
{
    public partial class UploadResult : Form
    {
        public MarksUpload h;
        public static SqlConnection con = Configuration.getInstance().getConnection();
        public int teacherId = 19;
        public string terrm;
        public static int classsId;
        string term = "";
        float score = 0.0f;
        string SESSION = "";
        public UploadResult(MarksUpload h)
        {
            this.h = h;

            InitializeComponent();
        
            fetching();
            PopulateComboBoxWithTerms();

        }

        public void insert()
        {
            classsId = int.Parse(ClassCB.Text.Trim());
            GetMarksData();
            term = guna2ComboBox1.Text.Trim();
            SESSION = guna2TextBox1.Text.Trim();
            bool isDuplicate = Queries.CheckDuplicate(classsId, term, SESSION);
            MessageBox.Show("Still Alive");
            if (isDuplicate)
            {
                MessageBox.Show("Data already exists for Class ID " + classsId + ", Term " + term + ", and Session " + SESSION);
             
                UplaodResult.DataSource= Queries.ShowSelectedResultData(classsId, SESSION,term);
            }
            else
            {
                TeacherMenuForm tmf = this.Tag as TeacherMenuForm;
                List<int[]> dataRows = Queries.ClassData(tmf.teacherId);
                MessageBox.Show("Still Alive");

                foreach (int[] row in dataRows)
                {
                    int studentID = row[0];
                    int subjectID = row[1];
                    int teacherID = row[2];
                    Queries.InsertResult(teacherID, subjectID, studentID, term, SESSION, score);
                }
                MessageBox.Show("Still Alive");

                UplaodResult.DataSource = Queries.ShowinsertedResultData(classsId, SESSION, term);
                MessageBox.Show("Still Alive");

            }
        }
    
        private void GetMarksData()
        {
            term = guna2ComboBox1.Text.Trim();
            SESSION = guna2TextBox1.Text.Trim();

            TeacherMenuForm tmf = this.Tag as TeacherMenuForm;
            List<int[]> dataRows = Queries.ClassData(tmf.teacherId);

            foreach (int[] row in dataRows)
            {
                int studentID = row[0];
                int subjectID = row[1];
                int teacherID = row[2];

                this.h = new MarksUpload(teacherID, subjectID, studentID, SESSION, term, score);
            }

        }
        private void AddBtn_Click(object sender, EventArgs e)
        {

        }

        private void guna2ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void PopulateComboBoxWithTerms()
        {
            guna2ComboBox1.Items.Clear();
            guna2ComboBox1.Items.Add("1st term");
            guna2ComboBox1.Items.Add("2nd term");
            guna2ComboBox1.Items.Add("3rd term");
        }
        void fetching()
        {
            try
            {
                ClassCB.Items.Clear();

                SqlCommand cmdFetchCLOIds = new SqlCommand("SELECT c.classID as Id FROM Class c", con);
                SqlDataReader reader = cmdFetchCLOIds.ExecuteReader();
                while (reader.Read())
                {
                    ClassCB.Items.Add(reader["Id"].ToString());
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error fetching IDs: " + ex.Message);
            }

        }
        private void ClassCB_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void guna2TextBox1_TextChanged(object sender, EventArgs e)
        {

        }
        public void cleardaata()
        {
            guna2TextBox1.Clear();
            guna2ComboBox1.SelectedIndex = -1;
            ClassCB.SelectedIndex = -1;
        }
        private void guna2Button1_Click(object sender, EventArgs e)
        {
            SESSION = guna2TextBox1.Text.Trim();
            if (string.IsNullOrEmpty(guna2ComboBox1.Text) || string.IsNullOrEmpty(guna2TextBox1.Text) || string.IsNullOrEmpty(ClassCB.Text))
            {
                MessageBox.Show("please Select all fields to Uplaod result");
                return;
            }
            if (!int.TryParse(SESSION, out int sessionYear) || sessionYear > DateTime.Now.Year)
            {
                MessageBox.Show("Please enter a valid session year , that is earlier than the current year.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                guna2TextBox1.Clear();
                return;
            }

            insert();
            cleardaata();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void HomeWorkView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        private void UplaodResult_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                int studentId = Convert.ToInt32(UplaodResult.Rows[e.RowIndex].Cells["StudentID"].Value);
                float updatedMarks;
                if (!float.TryParse(UplaodResult.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString(), out updatedMarks))
                {
                    MessageBox.Show("Invalid input. Please enter a valid number.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    UplaodResult.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = previousValue; 
                    return;
                }
                float totalmarks = Queries.GetTotalMarksForClass();
                if (updatedMarks > totalmarks)
                {
                    MessageBox.Show("Obtained marks cannot be greater than total marks.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    UplaodResult.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = previousValue;
                    return;
                }
                SqlCommand cmd = new SqlCommand(@"UPDATE Result
                                          SET ObtainedMarks = @ObtainedMarks
                                          WHERE StID = @StID", con);

                cmd.Parameters.AddWithValue("@ObtainedMarks", updatedMarks);
                cmd.Parameters.AddWithValue("@StID", studentId);
                cmd.ExecuteNonQuery();
            }
        }
        private float previousValue;
        private void UplaodResult_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (e.ColumnIndex >= 0 && UplaodResult.Columns[e.ColumnIndex].Name != "ObtainedMarks")
            {
                e.Cancel = true;
            }
            else if (e.ColumnIndex >= 0 && UplaodResult.Columns[e.ColumnIndex].Name == "ObtainedMarks")
            {
                previousValue = float.Parse(UplaodResult.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
            }
        }

        private void guna2ComboBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }
    }
}
