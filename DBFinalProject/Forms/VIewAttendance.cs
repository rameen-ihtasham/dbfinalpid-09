﻿using DBFinalProject.DataAccess; 
using Guna.UI2.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBFinalProject.Forms
{
    public partial class VIewAttendance : Form
    {
        public static int month;
        public VIewAttendance()
        {
            InitializeComponent();
       
        }

        public void loadGrid()
        {
            StudentMenuForm smf = this.Tag as StudentMenuForm;
            guna2DataGridView1.DataSource = Queries.ShowAttendance(smf.studentId);
        }
        private void guna2DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void guna2Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            try
            {

                month = int.Parse(ClassCB.Text.Trim());
                if (string.IsNullOrEmpty(ClassCB.Text))
                {
                    MessageBox.Show("please Select month to view month wise attendance");
                    return;
                }
                loadtotalgrid();
                ClassCB.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }





        }

        public void loadtotalgrid()
        {
            StudentMenuForm smf = this.Tag as StudentMenuForm;
            guna2DataGridView1.DataSource = Queries.ShowAttendancemonth(smf.studentId);
            guna2DataGridView2.DataSource=Queries.ShowAttendanceMonthwise(smf.studentId);
           
        }
        private void guna2DataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            try
            {
                loadGrid();
                guna2DataGridView2.DataSource = null;
                ClassCB.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void ClassCB_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void VIewAttendance_Load(object sender, EventArgs e)
        {
            loadGrid();
        }
    }
}
