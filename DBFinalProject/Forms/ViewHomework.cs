﻿using DBFinalProject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace DBFinalProject.Forms
{
    public partial class ViewHomework : Form
    {
        public ViewHomework()
        {
            InitializeComponent();
            //LoadHomeworkGrid();
        }


        public void LoadHomeworkGrid()
        {
        }

        private void ViewHomework_Load(object sender, EventArgs e)
        {
            StudentMenuForm smf = this.Parent as StudentMenuForm;
            HomeWorkView.DataSource = Queries.ShowhomeWorkData(smf.studentId);

        }
    }
}
