﻿using DBFinalProject.BL;
using DBFinalProject.DataAccess;
using Guna.UI2.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBFinalProject.Forms
{
    public partial class ViewResult : Form
    {
       
        public ViewResult()
        {
            InitializeComponent();
            loadgrid();
        }

        private void loadgrid()

        {
            guna2DataGridView1.DataSource= Queries.ShowResultData();

        }
        private void guna2DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
