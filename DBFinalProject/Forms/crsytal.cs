﻿using DBFinalProject.BL;
using DBFinalProject.CrystalReports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBFinalProject.Forms
{
    public partial class crsytal : Form
    {
        public crsytal()
        {
            InitializeComponent();
        }

        private void crsytal_Load(object sender, EventArgs e)
        {

        }
       
        private void guna2Button1_Click(object sender, EventArgs e)
        {
          
           ResultReport resultForm = new ResultReport();
            resultForm.ShowDialog();
        }

        private void guna2Button4_Click(object sender, EventArgs e)
        {
            AttendanceReport AttendanceForm = new AttendanceReport();
            AttendanceForm.ShowDialog();
        }

        private void guna2Button5_Click(object sender, EventArgs e)
        {
            Form f = new GenerateTotalStudents();
            f.ShowDialog();
        }
    }
}
