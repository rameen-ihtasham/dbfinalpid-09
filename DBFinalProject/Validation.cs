﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DBFinalProject
{
    public static class Validation
    {
        public static string GetValidEmail(string email)
        {
            string pattern = @"^([a-zA-Z0-9._%+-]+)@gmail\.com$";

            Regex regex = new Regex(pattern);

            Match match = regex.Match(email);

            if (match.Success)
            {
                return match.Value;
            }
            else
            {

                throw new ArgumentException("Invalid email input.");
            }
        }

        public static string GetValidCNIC(string cnic)
        {
            string pattern = @"^\d{5}-\d{7}-\d$";

            Regex regex = new Regex(pattern);

            Match match = regex.Match(cnic);

            if (match.Success)
            {
                return match.Value;
            }
            else
            {
                throw new ArgumentException("Invalid CNIC input.");
            }
        }
        public static string GetValidContact(string contact)
        {
            string pattern = @"^(\d{11})$";

            Regex regex = new Regex(pattern);

            Match match = regex.Match(contact);

            if (match.Success)
            {
                return match.Value;
            }
            else
            {
                throw new ArgumentException("Invalid contact input.");
            }
        }

    }
}
